<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\LoginForm;
use app\models\ForgotPasswordForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view','create','update','delete'],
                        'roles' => ['manageUsers'],

                    ],
                    [
                        // for guest
                        'allow' => true,
                        'actions' => ['index','signup','signin', 'forgot-password'],

                    ],

                ],
            ],

        ];
    }


 /*
    public function behaviors()
    {
        return [

        ];
    }
*/
// Колбек сработал! Эта страница может быть отображена только 31-ого октября
    public function actionSpecialCallback()
    {
        return $this->render('happy-halloween');
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        // $model->
        $model->load(Yii::$app->request->post());
        if ($model->validate()) {
           //$model->setPassword($model->password);
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //change role
            $this->assignRole($model);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new User();
        $model->scenario = User::SCENARIO_SIGNUP;


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            if ($model->save(false)) {

                $auth = Yii::$app->authManager;
                $authorRole = $auth->getRole('author');
                $auth->assign($authorRole, $model->getId());

                return $this->redirect('signin');
            } else {
                return $this->render('signup', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('signup', [
                'model' => $model,
            ]);
        }
        ////============================================
/*
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->login());
        //{
        //    return $this->goBack();
        //
        //}
        return $this->render('signup', [
            'model' => $model,
        ]);*/
    }

    public function actionSignin(){
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()){
            $this->redirect('/site/index');
        }else return $this->render('signin', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function assignRole($user)
    {
        $role = Yii::$app->request->post('role_id');

        $auth = Yii::$app->authManager;

        $items = $auth->getRoles($user->id);
        foreach ($items as $item) {
            $auth->revoke($item, $user->id);
        }
        //var_dump($user->id);
        //var_dump($auth->getRoles($user->getId()));
        
        // assign new role to the user
        $role_obj = $auth->getRole($role);

        $auth->assign($role_obj, $user->id);
        //$auth->save();
    }

    public function actionForgotPassword()
    {
        $model = new ForgotPasswordForm();
        return $this->render('forgot-password', [
            'model' => $model,
        ]);
    }


}
