<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $token
 * @property string $phone
 * @property string $date
 */
class User extends \yii\db\ActiveRecord  implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    const SCENARIO_SIGNIN = 'signin';
    const SCENARIO_SIGNUP = 'signup';
    

    public $passwordConfirmation = "";

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        /*
         * return [
            [['firstname', 'lastname', 'email', 'password', 'passwordConfirmation', 'phone'], 'required'],

            [['email'], 'email'],
            [['date',  'token'], 'safe'],
            [['firstname', 'lastname', 'email', 'phone'], 'string', 'max' => 30],
            [['password','passwordConfirmation', 'token'], 'string', 'max' => 255],

        /*
        сравнение пароля и подтверждения

        ['password', 'compare', 'compareAttribute' => 'passwordConfirmation'],
        ];
          */

        return [
            [['firstname', 'lastname', 'email',
                'password', 'passwordConfirmation', 'phone'], 'required', 'on' => self::SCENARIO_SIGNUP],

            [['email', 'password'], 'required', 'on' => self::SCENARIO_SIGNIN],

            [['email'], 'email'],
            [['date',  'token'], 'safe'],
            [['firstname', 'lastname', 'email', 'phone'], 'string', 'max' => 30],
            [['password','passwordConfirmation', 'token'], 'string', 'max' => 255],

            /*
            сравнение пароля и подтверждения
            */
            ['passwordConfirmation', 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_SIGNUP ],//????????????

            ['password', 'validateSigninPassword', 'on' => self::SCENARIO_SIGNIN],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateSigninPassword($attribute, $params)
    {
        
        //die();
        if (!$this->hasErrors()) {
            $user = $this;
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'phone' => 'Phone',
            'date' => 'Date',
        ];
    }



    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                  //  ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findByUserName($username){
        return static::findOne(['email' => $username]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
        // TODO: Implement findIdentity() method.
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
        return 0;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getRoles(){
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
     
            $this->setPassword($this->password);
     
            return true;
        }
        return false;
    }
    
}
