<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passwordConfirmation')->passwordInput(['maxlength' => true]) ?>

    <?/*= $form->field($model, 'salt')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'token')->textInput(['maxlength' => true])*/ ?>


    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?/*= $form->field($model, 'date')->textInput()*/ ?>

    <div class="form-group field-user-firstname">
    <label class="control-label" for="user-roles">Role</label>
    <?php
        $items = ArrayHelper::map(Yii::$app->authManager->roles,'name','description');
        echo Html::dropDownList("role_id", [reset($model->getRoles())->name], $items, ['class' => 'form-control']);?>
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?=$form->errorSummary($model)?>
    <?php ActiveForm::end(); ?>

</div>
