-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 11 2016 г., 16:57
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yiitest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '13', 1478942088),
('admin', '22', 1479553020),
('author', '14', 1478942088),
('author', '23', 1479553759),
('author', '3', 1479551427);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Admin', NULL, NULL, 1478942088, 1478942088),
('author', 1, 'Author', NULL, NULL, 1478942088, 1478942088),
('createPost', 2, 'Create a post', NULL, NULL, 1478942088, 1478942088),
('manageUsers', 2, 'Users Creating and Updating', NULL, NULL, 1478942088, 1478942088),
('updatePost', 2, 'Update post', NULL, NULL, 1478942088, 1478942088);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'author'),
('author', 'createPost'),
('admin', 'manageUsers'),
('admin', 'updatePost');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1478939345),
('m140506_102106_rbac_init', 1478939404);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `token`, `phone`, `date`) VALUES
(3, 'User21', 'User21', '123@dfgh.rt', '123', '', '123-23-23', '0000-00-00'),
(4, 'poi', 'poi', 'poi@poi.poi', 'poi', '', '123', '2016-11-04'),
(5, 'bnm', 'bnm', 'bnm@bnm.bnm', 'bnm', '', '123', '2016-11-04'),
(6, 'jkl', 'jkl', 'jlk@jkl.jkl', 'jkl', '', '123', '2016-11-04'),
(7, 'hgf', 'hgf', 'hgf@hgf.hgf', 'kjh', '', 'kjh', '2016-11-04'),
(8, 'khgkjg', 'hjgjhk', 'gjh@hgf.io', 'tre', '', 'tre', '2016-11-04'),
(9, 'erty', 'wty', 'weryt@rth.ty', '123', '', 'drthrth', '2016-11-04'),
(10, 'fthjfghj', 'fjfghfhg', 'fthef@fdgh.ty', '123', '', '123', '2016-11-04'),
(11, 'gkj', 'ghjk', 'ghkj@sdfg.dgf', 'tyu', '', 'ryt', '2016-11-04'),
(12, ';lk'''';', 'k;''lk;lk', 'hjgfg@fgh.er', '$2y$13$49/0Dmf4T7adhx1KTSXB9.OBCzV.kg8.V7/pTmx8oaptsPfFWU9Vq', '', 'ewasadag', '2016-11-08'),
(13, 'user1', 'user1', 'user1@mail.ua', '$2y$13$YEL.quvGvpf/QzZ.BPSFJ.V28Ra.wIqXyrT3q.rNtOgUsdteKBeQ6', '', '223322', '2016-11-11'),
(14, 'user2', 'user2', 'user2@mail.ua', '$2y$13$zQeEt4COVkRE92CCTNJRn.BUfa36O0tQ8FrGlmp1.v1j7OlX/wBRe', '', '223322', '2016-11-11'),
(15, '', '', '', '$2y$13$TRJstaXolWZ3fUc71mQ4auqSwHlljRH4ygzyXA3k2i9Kt8.GzEEki', '', '', '2016-11-15'),
(16, '', '', '', '$2y$13$KrXFLEhUrbSrULwPw9IelO2t1YwiSbR3IFsDqAYFAc/BWOdIglb.S', '', '', '2016-11-15'),
(17, '', '', '', '$2y$13$FWL94uefcSiUk4fI/c1UzuSqGWgVI7fa22x20gY/BVPrcFoeqIK36', '', '', '2016-11-15'),
(18, '', '', '', '$2y$13$EADzexi1fTwDGIsgrkiPrOUcVNmNfXYayQEcY9SfQSMQ3MI/a40im', '', '', '2016-11-15'),
(19, '', '', '', '$2y$13$L1Qsth33FrxVYSUZqORFsec6WItBxvJQ0VftTSANcQMYpIAumKy/u', '', '', '2016-11-15'),
(20, '', '', '', '$2y$13$qcMLz34eKa3tAgsEvxm5Cewxx8oWACTmWYrsMvBYlJUKSjEC9wMHG', '', '', '2016-11-15'),
(21, 'qweqewqewqeq', 'ewqeqewqe', 'qeqeqeqwe@dgj.er', '$2y$13$xvJZKyixX9mQTt6kDAV95.6xDN8Cg9L7r48LRGVVRRH1O.2ab/mv6', '', '34563566745675', '2016-11-15'),
(22, 'author22', 'author22', 'author22@mail.ua', 'author22', '', '123', '2016-11-19'),
(23, 'gogogo', 'gogogo', 'gogogo@mail.ua', '$2y$13$R5VP0UUHMel6Q6A9askVwe2J2iVu7q2jMrOhkca5uPEkJUvxpTEHm', '', '123', '2016-11-19');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
